const fs = require('fs');
const cheerio = require('cheerio');
const cheerioTableparser = require('cheerio-tableparser');
const fetch = require('node-fetch');


const games = [];

fetch('https://en.wikipedia.org/wiki/List_of_best-selling_video_games')
  .then(res => res.text())
    .then(html => {
        const $ = cheerio.load(html);
        cheerioTableparser($);

        // parsetable makes handling scraping tables easy as pie.
      const gameTable =  $('.wikitable').parsetable(false,false,true);

      for (var i = 1; i < gameTable[0].length; i++) {

        let game = {
          rank: gameTable[0][i],
          title: gameTable[1][i],
          sales: gameTable[2][i],
          platform: gameTable[3][i],
          releaseDate: gameTable[4][i],
          developer: gameTable[5][i],
          publisher: gameTable[6][i]
        };

      games.push(game)
      }

       fs.writeFile(`./games.json`, JSON.stringify(games), (err) => {
         if (err) throw err;

         console.log("saved games as JSON")
       });
    })
        .catch(err => console.log(err));
